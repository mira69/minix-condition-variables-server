#include "inc.h"

endpoint_t who_e;
message m;

static struct {
	int type;
	int (*func)();
} cs_calls[] = {
	{ CS_LOCK,	do_lock		},
	{ CS_UNLOCK,	do_unlock	},
	{ CS_WAIT,	do_wait		},
	{ CS_BROADCAST,	do_broadcast	},
};

/* SEF functions and variables. */
static void sef_local_startup(void);
static int sef_cb_init_fresh(int type, sef_init_info_t *info);
static void sef_cb_signal_handler(int signo);

/*===========================================================================*
 *				     main				     *
 *===========================================================================*/
int main(int argc, char *argv[])
{
	int call_type;

	/* SEF local startup. */
	env_setargs(argc, argv);
	sef_local_startup();

	while (TRUE) {
		int r;
		int cs_number;

		if ((r = sef_receive(ANY, &m)) != OK)
			printf("sef_receive failed %d.\n", r);
			//Why does ipc interpret the message even when the receive fails?
		who_e = m.m_source;
		call_type = m.m_type;

		if (call_type & NOTIFY_MESSAGE) {
			switch (who_e) {
			default:
				printf("CS: ignoring notify() from %d\n", who_e);
				break;
			}
			continue;
		}

		if (who_e == PM_PROC_NR){
			int handled = 1;
			switch (call_type) {
			case PM_DUMPCORE:
			case PM_EXIT:
				who_e = m.PM_PROC;
				q_remove();
				break;
			case PM_UNPAUSE:
				who_e = m.PM_PROC;
				unpause();
				break;
			default:
				handled = 0;
				break;
			}
			if (handled){
				continue;
			}
		}

		cs_number = call_type - (CS_BASE + 1);

		/* dispatch message */
		if (cs_number >= 0 && cs_number < SIZE(cs_calls)) {
			if (cs_calls[cs_number].type != call_type)
				panic("CS: call table order mismatch");

			if ((r = cs_calls[cs_number].func()) != SUSPEND){
				send_msg(who_e, r);
			}
		} else {
			/* warn and then ignore */
			printf("CS unknown call type: %d from %d.\n", call_type, who_e);
		}
	}

	return -1;
}

/*===========================================================================*
 *			       sef_local_startup			     *
 *===========================================================================*/
static void sef_local_startup()
{
  /* Register init callbacks. */
  sef_setcb_init_fresh(sef_cb_init_fresh);
  sef_setcb_init_restart(sef_cb_init_fresh);

  /* No live update support for now. */

  /* Register signal callbacks. */
  sef_setcb_signal_handler(sef_cb_signal_handler);

  /* Let SEF perform startup. */
  sef_startup();
}

/*===========================================================================*
 *		            sef_cb_init_fresh                                *
 *===========================================================================*/
static int sef_cb_init_fresh(int UNUSED(type), sef_init_info_t *UNUSED(info))
{
/* Initialize the CS server. */

    init_queues();

    return(OK);
}

/*===========================================================================*
 *		            sef_cb_signal_handler                            *
 *===========================================================================*/
static void sef_cb_signal_handler(int signo)
{
  /* Only check for termination signal, ignore anything else. */
  if (signo != SIGTERM) return;
}

