#include "inc.h"


/*===========================================================================*
 *				do_lock		     		     *
 *===========================================================================*/
int do_lock(){
	int mutex_id = m.LOCK_MUTEX_ID;
	if (try_grant(who_e, mutex_id)) {
		return OK;
	}
	return SUSPEND;
}
/*===========================================================================*
 *				do_unlock		     		     *
 *===========================================================================*/
int do_unlock(){
	int mutex_id = m.UNLOCK_MUTEX_ID;
	endpoint_t next = who_e;
	if (!try_free(mutex_id, &next)){
		return EPERM;
	}
	if (next != who_e){
		send_msg(next, OK);
	}
	return OK;
}
/*===========================================================================*
 *				do_wait		     		     *
 *===========================================================================*/
int do_wait(){
	int cv_id = m.WAIT_CV_ID;
	int mutex_id = m.WAIT_MUTEX_ID;
	endpoint_t next = who_e;

	int r = wait_cv(cv_id, mutex_id, &next);
	if (r != OK){
		return r;
	}

	if (next != who_e){
		send_msg(next, OK);
	}

	return SUSPEND;
}
/*===========================================================================*
 *				do_broadcast		     		     *
 *===========================================================================*/
int do_broadcast(){
	int cv_id = m.BROADCAST_CV_ID;
	endpoint_t ready[MAX_PROC_COUNT];
	int size = wake_up(cv_id, ready, MAX_PROC_COUNT);

	for (int i = 0; i < size; i++){
	    send_msg(ready[i], OK);
	}
	return OK;
}
/*===========================================================================*
 *				send_msg		     		      *
 *===========================================================================*/
void send_msg(endpoint_t dst, int result){
	m.m_type = result;
	int r = sendnb(dst, &m);
	if (r != OK) {
		printf("CS send error %d.\n", r);
	}
}
