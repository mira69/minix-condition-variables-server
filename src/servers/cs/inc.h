#ifndef _CS_INC_H
#define _CS_INC_H

#define _POSIX_SOURCE      1    /* tell headers to include POSIX stuff */
#define _MINIX             1    /* tell headers to include MINIX stuff */
#define _SYSTEM            1    /* get OK and negative error codes */

#include <minix/callnr.h>
#include <minix/com.h>
#include <minix/const.h>
#include <minix/config.h>
#include <minix/endpoint.h>
#include <minix/ipc.h>
#include <minix/syslib.h>
#include <minix/sysutil.h>

#include <sys/types.h>

#include <errno.h>
#include <signal.h>
#include <stdio.h>

#define SIZE(a) (sizeof(a)/sizeof(a[0]))

#define MAX_MUTEX_COUNT 1024
#define MAX_PROC_COUNT _NR_PROCS

int do_lock();
int do_unlock();
int do_wait();
int do_broadcast();
void send_msg(endpoint_t dst, int result);

EXTERN endpoint_t who_e;
EXTERN message m;

int try_grant(endpoint_t who, int id);
int try_free(int id, endpoint_t *next);
int wait_cv(int cv_id, int mutex_id, endpoint_t *next);
int wake_up(int id, endpoint_t *ready, const int size);
void init_queues();
void q_remove();
void unpause();

#endif /* _CS_INC_H */
