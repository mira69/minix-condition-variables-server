#include "inc.h"

static struct mutex_t {
	int id;
	int granted;
	endpoint_t owner;
	endpoint_t *queue;
	int q_beg;
	int q_used;
	int q_size;
} mutex[MAX_MUTEX_COUNT];

static struct cv_t {
	int id;
	struct cv_q_t {
		endpoint_t who;
		int mutex_id;
	} *queue;
	int q_used;
	int q_size;
} cv[MAX_PROC_COUNT];

static endpoint_t mutex_q[MAX_MUTEX_COUNT*MAX_PROC_COUNT];
static struct cv_q_t cv_q[MAX_PROC_COUNT*MAX_MUTEX_COUNT];

static int find_m(int mutex_id);
static int find_cv(int cv_id);

/*===========================================================================*
 *				 init_queues				     *
 *===========================================================================*/
void init_queues(){
	struct mutex_t base_m = {.granted = 0, .q_beg = 0, .q_used = 0, .q_size = MAX_PROC_COUNT};

	for (int i = 0; i < MAX_MUTEX_COUNT; i++){
		mutex[i] = base_m;
		mutex[i].queue = mutex_q + (i * MAX_PROC_COUNT);
	}

	struct cv_t base_c = {.q_used = 0, .q_size = MAX_MUTEX_COUNT};

	for (int i = 0; i < MAX_PROC_COUNT; i++){
		cv[i] = base_c;
		cv[i].queue = cv_q + (i*MAX_MUTEX_COUNT);
	}
}

/*===========================================================================*
 *				   try_grant				     *
 *===========================================================================*/
int try_grant(endpoint_t who, int id){
	int pos = find_m(id);
	struct mutex_t *q;
	if (pos < 0){
		return FALSE;
	}
	q = &(mutex[pos]);
	if (!q->granted){
		q->id = id;
		q->granted = 1;
		q->owner = who;
	}
	if (q->owner == who){
		return TRUE;
	}
	if (q->q_used < q->q_size){
		q->queue[(q->q_beg + q->q_used) % q->q_size] = who;
		q->q_used++;
	}
	return FALSE;
}

/*===========================================================================*
 *				   try_free				     *
 *===========================================================================*/
int try_free(int id, endpoint_t *next){
	int pos = find_m(id);
	struct mutex_t *q;
	if (pos < 0 || !(q = &mutex[pos])->granted || q->owner != who_e){
		return FALSE;
	}
	q = &(mutex[pos]);
	if (q->q_used == 0){
		q->granted = 0;
	}
	else {
		q->owner = q->queue[q->q_beg];
		q->q_beg = (q->q_beg + 1) % q->q_size;
		q->q_used--;
		*next = q->owner;
	}
	return TRUE;
}

/*===========================================================================*
 *				   wait_cv				     *
 *===========================================================================*/
int wait_cv(int cv_id, int mutex_id, endpoint_t *next){
	int pos = find_cv(cv_id);
	struct cv_t *q;
	if (pos < 0 || cv[pos].q_used == cv[pos].q_size){
		return OK;
	}
	q = &cv[pos];
	if (!try_free(mutex_id, next)){
		return EINVAL;
	}
	q->id = cv_id;
	q->queue[q->q_used++] = (struct cv_q_t) {who_e, mutex_id};
	return OK;
}

/*===========================================================================*
 *				   wake_up				     *
 *===========================================================================*/
int wake_up(int id, endpoint_t *ready, const int size){
	int pos = find_cv(id);
	struct cv_t *q;
	int used = 0;
	if (pos < 0){
		return 0;
	}
	q = &cv[pos];
	for (int i = 0; i < q->q_used; i++){
		if (try_grant(q->queue[i].who, q->queue[i].mutex_id) && used < size){
		    ready[used++] = q->queue[i].who;
		}
	}
	q->q_used = 0;
	return used;
}

/*===========================================================================*
 *				   q_remove				     *
 *===========================================================================*/
void q_remove(){
	for (struct mutex_t *p = mutex; p < mutex + MAX_MUTEX_COUNT; p++){
		if (!p->granted){
			continue;
		}
		if (p->owner == who_e){
			if (!p->q_used){
				p->granted = 0;
			}
			else {
				p->owner = p->queue[p->q_beg];
				p->q_beg = (p->q_beg + 1) % p->q_size;
				p->q_used--;
				send_msg(p->owner, OK);
			}
			continue;
		}
		for (int i = 0; i < p->q_used; i++){
			if (p->queue[(i + p->q_beg) % p->q_size] == who_e){
				p->q_used--;
				for (int j = i; j < p->q_used; j++){
					p->queue[(j + p->q_beg) % p->q_size] = p->queue[(j + 1 + p->q_beg) % p->q_size];
				}
				break;
			}
		}
	}
	for (struct cv_t *p = cv; p < cv + MAX_PROC_COUNT; p++){
		for (int i = 0; i < p->q_used; i++){
			if (p->queue[i].who == who_e){
				p->q_used--;
				struct cv_q_t tmp = p->queue[i];
				p->queue[i] = p->queue[p->q_used];
				p->queue[p->q_used] = tmp;
				return;
			}
		}
	}
}

/*===========================================================================*
 *				   unpause				     *
 *===========================================================================*/
void unpause(){
	for (struct mutex_t *p = mutex; p < mutex + MAX_MUTEX_COUNT; p++){
		for (int i = 0; i < p->q_used; i++){
			if (p->queue[(i + p->q_beg) % p->q_size] == who_e){
				p->q_used--;
				for (int j = i; j < p->q_used; j++){
					p->queue[(j + p->q_beg) % p->q_size] = p->queue[(j + 1 + p->q_beg) % p->q_size];
				}
				send_msg(who_e, EINTR);
				return;
			}
		}
	}
	for (struct cv_t *p = cv; p < cv + MAX_PROC_COUNT; p++){
		for (int i = 0; i < p->q_used; i++){
			if (p->queue[i].who == who_e){
				p->q_used--;
				struct cv_q_t tmp = p->queue[i];
				p->queue[i] = p->queue[p->q_used];
				p->queue[p->q_used] = tmp;
				send_msg(who_e, EINTR);
				return;
			}
		}
	}
}

/*===========================================================================*
 *				  utilities				     *
 *===========================================================================*/
static int find_m(int mutex_id){
	int pos = -1;

	for (int i = 0; i < SIZE(mutex); i++){
		if (mutex[i].id == mutex_id){
			return i;
		}
		if (!mutex[i].granted){
			pos = i;
		}
	}

	return pos;
}

static int find_cv(int cv_id){
	int pos = -1;

	for (int i = 0; i < SIZE(cv); i++){
		if (cv[i].id == cv_id){
			return i;
		}
		if (!cv[i].q_used){
			pos = i;
		}
	}

	return pos;
}
