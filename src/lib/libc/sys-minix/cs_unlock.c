#define _SYSTEM 1
#define _MINIX 1
#include <sys/cdefs.h>
#include "namespace.h"
#include <lib.h>

#include <minix/rs.h>

#include <cv.h>
#include <errno.h>

static int get_cs_endpt(endpoint_t *pt)
{
	return minix_rs_lookup("cs", pt);
}

int cs_unlock(int mutex_id)
{
	message m = {.UNLOCK_MUTEX_ID = mutex_id};
	endpoint_t cs_pt;

	if (get_cs_endpt(&cs_pt) != OK){
		errno = ENOSYS;
		return -1;
	}

	return _syscall(cs_pt, CS_UNLOCK, &m);
}
