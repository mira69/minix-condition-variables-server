#define _SYSTEM 1
#define _MINIX 1
#include <sys/cdefs.h>
#include "namespace.h"
#include <lib.h>

#include <minix/rs.h>

#include <cv.h>
#include <errno.h>

static int get_cs_endpt(endpoint_t *pt)
{
	return minix_rs_lookup("cs", pt);
}

int cs_wait(int cond_var_id, int mutex_id)
{
	message m = {.WAIT_CV_ID = cond_var_id, .WAIT_MUTEX_ID = mutex_id};
	endpoint_t cs_pt;
	int r;

	if (get_cs_endpt(&cs_pt) != OK){
		errno = ENOSYS;
		return -1;
	}

	if ((r = _syscall(cs_pt, CS_WAIT, &m)) == -1 && errno == EINTR){
		return cs_lock(mutex_id);
	}

	return r;
}
